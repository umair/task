<?php
/**
 * Created by PhpStorm.
 * User: Umair Anwar
 * Date: 12/13/2017
 * Time: 9:38 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskController extends Controller
{
    public function getList() {

        // The second parameter is used to specify on what object the role is tested.
        $this->denyAccessUnlessGranted(
            'ROLE_USER',
            null,
            'Unable to access this page!'
        );

        return $this->render('tasks.html');
    }
}